# -*- coding: utf-8 -*-
from scipy import polyval, polyfit
from SimPy.Simulation import *
from random import expovariate #, seed
import matplotlib.pyplot as plt
from SimPy.SimPlot import *
import numpy as np
from random import Random,expovariate  


time1 = now()

""" """
class Client(Process):
	"""docstring for ClassName"""
	inserv = 0
	clients = 0 
	clientsServed = 0
	clientsHappy = 0 
	def __init__(self):
		Process.__init__(self)
		self.id = "Client " + str(Client.clients)

	""" QoS: how well it served? """

	def wellserved(self, servtime, stime, rate):
		happytime = 1/(stime-rate)
		if servtime < happytime:
			return "happy"
		else:
			return "unhappy"

	""" Обслуживание в ленивых вычислениях """
	def toserv(self, stime, rate):
		arrtime = now()
		self.inserv += 1
		#print 's_ins', self.inserv
		yield request, self, server
		timetime = expovariate(stime)   
		if random.choice([1]) == 1:

			yield hold, self, timetime

			gT.observe(now())
		else: 
			yield hold, self, timetime
			#nT.observe(now())

		yield release, self, server
		
		#sT.observe(timetime)
		Client.clientsServed+=1
		newtime = now()
		m.observe(self.inserv)
		#print 'inserv ', self.inserv
		self.inserv -=1
		#print 's_ins', self.inserv
		servtime = newtime - arrtime 

		mT.observe(servtime) 
		if self.wellserved(servtime, stime, rate)=="happy" : Client.clientsHappy += 1
	#print 'inserv ', inserv

	def toserv2(self, stime, rate):
		arrtime = now()
		self.inserv += 1
		#print 's_ins', self.inserv
		yield request, self, server
		timetime = expovariate(stime) #5  
		yield hold, self, timetime
		yield release, self, server
		
		sT.observe(timetime)
		Client.clientsServed+=1
		newtime = now()
		m.observe(self.inserv)
		self.inserv -=1
		#print 's_ins', self.inserv
		servtime = newtime - arrtime 

		#mT.observe(servtime) 
		if self.wellserved(servtime, stime, rate)=="happy" : Client.clientsHappy += 1
		

class Traffic(Process):
	""" Входной поток, ленивый """
	def __init__(self, rate):
		#super(ClassName, self).__init__()
		self.rate = 1
		Process.__init__(self)
	def generator(self, rate):
		while True:
			cust = Client()

			activate(cust, cust.toserv(stime, rate), delay=0) #if random.randint(1,2) == 1 else activate(cust, cust.toserv2(stime, rate) delay=0)
			yield hold, self, expovariate(rate)

class Traffic2(Process):
	""" Входной поток, ленивый """
	def __init__(self, rate):
		#super(ClassName, self).__init__()
		self.rate = 1
		Process.__init__(self)
	def generator(self, rate):
		while True:
			cust = Client()
			activate(cust, cust.toserv2(stime, rate), delay=0)
			yield hold, self,  expvariate(rate)

## Experiment datamo

TRACING = True
stime = 0.2			#mju
rate = 0.1		#lambda
#seed(1434524345674721223400074)


mT  = Monitor()  ## monitor for the time in system
m   = Monitor()  ## monitor for the number of jobs
sT  = Monitor()
gT  = Monitor()


## Simulate
initialize()

server=Resource(capacity=1)
traffic = Traffic(rate)
#traffic2= Traffic2(rate)

#activate(traffic, traffic.generator(rate)) if random.randint(1,2) == 1 else activate(traffic2, traffic2.generator(rate))
activate(traffic, traffic.generator(rate))
#activate(traffic2, traffic2.generator(rate))

simulate(until=100000)	#hours


## Visualizing

def getG(x):
	g = []
	g0 = []
	g00 = []
	for i in x:
		
		#g.append(i[1])
		g0.append(i[1])
		g00.append(i[0])
	#print g0	
	ll = len(g0)
	g01 = [0] + g0[:ll-1]
	sbs = np.subtract(g0, g01)
	#print 'sbs ', sbs[1:200]

	dfh = plt.hist(sbs, normed=True, histtype='step', bins=np.arange(0, (max(g0)+0.1), 0.1)) 
	#inter_x = polyfit(range(0, len(g0)), g0, 3)
	q = np.array(sbs)
	j = np.array(g00)
	inter_x = np.polyfit(q[0:100], j[0:100], 30)

	sbshow = plt.plot(sbs)
	approx = plt.plot(inter_x)
	#(max(g0)+0.1) , 0.1
	#nph = np.histogram(g0, bins=300, range=None, normed=1, weights=None, density=None)
	return  'dfh ', dfh, 'gfh[0] ', dfh[0],   plt.show(), 'var ', np.std(dfh[0]), 'expected value', dfh[0].mean(), 'summ ', dfh[0].sum()*0.1
	#, g0 plt.plot(g0), plt.show(),#, 'nph ',nph#, dfh[0].var(), 'expected value', dfh[0].mean() #, 'length of  ', len(g0) 
	#  plt.hist(g0, bins=1000),   plt.show(), 'g0    ', g0 #, 'g0  '
	#, g0#, plt.plot(g), plt.show(),   , g, 
  	#''' variance ', 'sum ',dfh[0].sum(), max(g0)/300''' 
time2 = now()

def variance(x):
	g = []
	m = x.mean()
	s = 0 
	for i in x:
		s += ((i[1])-m)**2
	return 'var ', s/len(x) 

#print 'getG  ', getG(mT)
print 'getS  ', getG(gT)
#print 'gT ', gT
print ("Среднее время пребывания заявки в системе   {0:6.4f}".format(mT.mean()))
print ("Среднее число заявок в очереди   {0:6.4f}".format(m.mean()))
print Client.clientsServed, Client.clientsHappy

#print 'timetime ', time2 - time1
#print 'mT  ', mT
print 'var mT ', variance(mT)
print 'mean mT ', mT.mean()
#print 'var sT ', variance(sT)
#print 'mean sT ', sT.mean()
print '--- --- --- '



class Density(object):
    '''
    Класс распределение.
    Наброски. На данный момент я себе его так представляю.
    '''

    def __init__(self, y, mu):
        '''
        Инициализация распределения.
        Код Александра по генерации распределния.
        '''
        self.__dt = 0.1 # шаг распределения
        self.__p = [] # массив со значениями распределения

        # Для примера - аналитическое распределение
        x = mu-y
        t = np.arange(0.0, 10.0, self.__dt);
        self.__p = x*np.exp(-x*t)
        # Окончание примера

        self.__len = len(self.__p)
        self.__y = [] # массив входных значений, заполняется по мере обращения
        self.__t = 0.0 # текущее время
    
    def p(self, t):
        '''
        Интерполяция сгенерированного распределения.
        В идеале, через данную функцию реализуется доступ к значениям self.__p
        '''
        point = t/self.__dt
        p1 = int(np.fix(point))
        if p1 > self.__len - 1:
            return 0.0
        dt = point - p1
        p2 = p1 + 1
        p1 = self.__p[p1]
        if p2 > self.__len - 1:
            p2 = 0.0
        else:
            p2 = self.__p[p2]
        p = p1 + (p2 - p1) * dt
        return p
    
    def y(self, y_in):
        '''
        Свёртка входного сигнала и распределения.
        Каждое обращение к этой фцнкции подразумевает смещение времени на некоторое значение.
        Пускай сначала оно будет равно self.__dt, интервалу генерации распределения.
        Код Аллы, реализующий свёртку.
        '''
        #Добавляем новое значение к входному сигналу
        self.__y.append(y_in)

        # дальше свёртка self.__y и self.__p
        y_out = self.__y[0]*self.p(self.__t) # просто пример-заглушка

        # Изменили текущее время
        self.__t += self.__dt

        return y_out

#Пример использования.
#Создание
d = Density(rate, stime)
#Значение распределения в точке 0.05
#print(d.p(0.05))
#Значение выхода в момент времени t = 0.0
#print(d.y(1.0))
#Значение выхода в момент времени t = 0.1 (0.0 + dt)
#print(d.y(1.0))
#Значение выхода в момент времени t = 0.2 (0.1 + dt)
#print(d.y(0.0))



# Описать эксперимент, и данные занести в сводную таблицу.
 